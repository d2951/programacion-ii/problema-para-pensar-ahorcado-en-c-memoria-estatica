# Problema para pensar - Ahorcado en C - Memoria estática

## Compilación

gcc main.c entrada.c ahorcado.c

## Queda como ejercicio

- verificar que las letras que se ingresan no se hayan ingresado antes
- mostrar las letras ingresadas hasta el momento en cada turno
