#include "ahorcado.h"

void ahorcado(char palabra_secreta[], char resultado[]) {

  char estado_juego[LARGO_MAXIMO_PALABRA];
  int vidas = VIDAS_AHORCADO;

  iniciar_tablero(palabra_secreta, estado_juego);
  
  mostrar_tablero(estado_juego);
  mostrar_vidas(vidas);

  int continua = 1;
  int acierto = -1; // Vale 0 si se acierta una letra. -1 en caso contrario.
  char letra;

  while(!juego_ganado(palabra_secreta, estado_juego) && vidas > 0 && continua) {
    letra = pedir_letra();
    continua = juego_continua(letra);

    if (continua) {
      acierto = modificar_tablero(palabra_secreta, estado_juego, letra);
      vidas += acierto;
    }
    
    mostrar_tablero(estado_juego);
  }

  if (vidas == 0) {
    strcpy(resultado, "Perdiste! Te quedaste sin vidas");
    return;
  } else if (!continua) {
      strcpy(resultado, "Terminaste el juego");
      mostrar_tablero(estado_juego);
      return;
  }
  
  generar_mensaje_ganador(resultado, vidas);
}

void iniciar_tablero(char palabra_secreta[], char estado_juego[]) {
  int largo_palabra = strlen(palabra_secreta);

  for (int i = 0; i < largo_palabra; i++) {
    estado_juego[i] = '-';
  }

  estado_juego[largo_palabra] = '\0';
}

void mostrar_tablero(char palabra_secreta[]) {
  system(limpiar);
  printf("\n<< %s >>\n\n", palabra_secreta);
}

void mostrar_vidas(int vidas) {
  printf("Cantidad de vidas restantes: %d\n", vidas);
}

char pedir_letra() {
  char letra;

  printf("Ingrese una letra para jugar (o 1 para abandonar): ");
  scanf(" %c", &letra); // IMPORTANTE: el espacio se utiliza para ignorar espacios y endlines
  printf("pedir letra: %c\n", letra);

  return letra;
}

int juego_continua(char letra) {
  return !(letra == '1');
}

int modificar_tablero(char palabra_secreta[], char estado_juego[], char letra) {

  int largo_palabra = strlen(palabra_secreta);
  int modificaciones = 0;

  for (int i = 0; i < largo_palabra; i++) {
    if (palabra_secreta[i] == letra) {
      estado_juego[i] = letra;
      modificaciones++;
    }
  }

  if (modificaciones) {
    return 0;
  }
  return -1;

}

int juego_ganado(char palabra_secreta[], char estado_juego[]) {
  return strcmp(palabra_secreta, estado_juego) == 0;
}

void generar_mensaje_ganador(char resultado[], int vidas) {
  char m1[LARGO_MAXIMO_MENSAJE] = "Ganaste! Te sobraron";
  char m2[] = "vidas";

  sprintf(resultado, "%s <%d> %s", m1, vidas, m2);
}