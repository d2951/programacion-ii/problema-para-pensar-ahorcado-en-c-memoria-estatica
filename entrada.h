#ifndef ENTRADA_H
#define ENTRADA_H

#define LARGO_MAXIMO_PALABRA 20
#define LARGO_MAXIMO_LINEA 50
#define CANT_MAX_PALABRAS 50

#include <time.h> // Generar numero aleatorio
#include <stdlib.h> // Generar numero aleatorio
#include <stdio.h> // Leer archivo
#include <string.h> // Copiar un string (strcpy)
#include <assert.h> // Verificar que el archivo se abra correctamente

/*
 * abrir_archivo toma un nombre de archivo y un modo para abrirlo
 * y devuelve el objeto
 */
FILE* abrir_archivo(const char nombre_archivo[], char modo[]);

/*
 * elegir_palabra toma una cadena que representa el nombre
 * del archivo de palabras y devuelve una tomada al azar 
 */
void elegir_palabra(const char *nombre_archivo, char palabra_secreta[]);


/*
 * numero_aleatorio toma 2 enteros (min, max) y devuelve
 * un número aleatorio en el intervalo [min, max]
 */
int numero_aleatorio(int minimo, int maximo);

#endif
